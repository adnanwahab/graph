import GraphRenderer from '../src';
import * as d3 from 'd3'
import _ from 'lodash'

let canvas = document.createElement('canvas')

let main = () => {


  document.body.appendChild(canvas)

  load('./data/thecut1.json')

  document.title = 'REGL NETWORK VIS'
}

let brush = (container) => {
  let width = innerWidth, height = innerHeight

  const svg = d3.select(container).append("svg")
      .attr("viewBox", [0, 0, width, height])
      .property("value", [])
      svg.style(
        'position', 'absolute'
      )
      svg.style('top', '50')
      svg.style('left', '0')


  const brush = d3.brush()
      .on("end", brushed)
      // .on('end', ()=> {
      //
      //   svg.selectAll('.selection')
      //   .transition().duration(500)
      //   .ease(d3.easeLinear)
      //   .attr('opacity', 0)
      //
      // })

  // const dot = svg.append("g")
  //     .attr("fill", "none")
  //     .attr("stroke", "steelblue")
  //     .attr("stroke-width", 1.5)
  //   .selectAll("g")
  //   .data(data)
  //   .join("circle")
  //     .attr("transform", d => `translate(${x(d.x)},${y(d.y)})`)
  //     .attr("r", 3);

  svg.call(brush);
  svg.selectAll('.selection').attr('stroke', 'green')//.attr('fill', 'dark-green').attr('fill-opacity', .3)

  function brushed() {
    svg.selectAll('.selection').attr('opacity', 1).attr('fill', 'none')

    let value = [];
    if (d3.event.selection) {
      graph.brush(d3.event.selection, svg.node())
      //value = data.filter(d => x0 <= x(d.x) && x(d.x) < x1 && y0 <= y(d.y) && y(d.y) < y1);
    }
    //svg.property("value", value).dispatch("input");
  }
}

let styles = {
    'font-size': '16px',
    'font-weight': '500',
    'background-color': 'rgb(41, 50, 60)',
    'color': 'rgb(160, 167, 180)',
    'z-index': '1001',
    'position': 'fixed',
    'overflow-x': 'auto',
    'top': '541px',
    'max-width': '1000px',
    'right': '185px',
    'width': '200px',
    'display': 'flex',
    'box-sizing': 'border-box',
    'max-width': '100%',
    'color': 'rgb(248, 248, 248)',
    'min-width': '0px',
    'min-height': '0px',
    'flex-direction': 'column',
    'outline': 'none',
    'margin': '6px',
    'background': 'rgb(119, 119, 119)',
    'padding': '12px',
    'opacity': '.8',
    'pointer-events': 'none'

}

let favorites = []
let load = (url) => {
  fetch(url)
    .then((body)=>{ return body.json() })
    .then((json)=>{
        window.graph = GraphRenderer.init({
          data: json,
          canvas: canvas,
          width: innerHeight,
          height: innerHeight,

          // onClick: (point, idx, events) => {
          //   if (events.shiftKey)favorites = favorites.concat(idx)
          //   graph.setState({favorites})
          // }
        })
        // graph.setState({flatSize: false})
        let parseColor = (rgb) => {
          let c = d3.rgb(rgb)
          return [c.r /255 , c.g /255 , c.b /255];
        }
          json.cluster_events.forEach((c) => {
            c.clusters.forEach((cluster, clusterIndex) => {
              graph.setNodeColor(cluster.nodes, cluster.color)
            })
          })

        Array.from(document.body.querySelectorAll('input')).forEach(el => {
          el.addEventListener('change', (e) => {
            let x = {}
            x[el.id] = + e.target.value
            graph.setState(x)
            console.log(x

            )
          })
        })

        //brush(document.body)
         //
         // graph.on('wheel', (delta) => {
         //   graph.setState({sizeAttenuation: delta / 1000})
         // })
        // graph.setState({sizeAttenuation: 1})

        //graph.on('hover', (d) => {console.log(d)})
        //
        // d3.selectAll(document.body).call(d3.zoom()
        //       .extent([[0, 0], [innerWidth, innerHeight]])
        //       //.scaleExtent([1, 500])
        //       .on("zoom", zoomed))

        let tip = d3.select('body').append('div')

        _.each(styles, (key, value) => { tip.style(value, key)})

        graph.on('hover', (i, node, event, coordinates) => {
          if (! node) return console.log('off')
          tip.text(node.text)
          tip.style('display', 'block')
          tip.style('left', coordinates[0] + 'px')
          tip.style('top', coordinates[1] + 'px')

        })

        graph.on('hoverOff', (i, node, event, coordinates) => {

          tip.style('display', 'none')
        })


    })
}




d3.select(window).on('load', main)
