import KDBush from 'kdbush'
import withThrottle from 'lodash-es/throttle'
import withRaf from 'with-raf'
import { mat3, vec3 } from 'gl-matrix'
import _ from 'lodash'

import processData from './processData';
import createCurves from './curves'

import createDrawPoints from './points'

import circleSprite from './sprites/border.png'
import starSprite from './sprites/lol.jpg'

import * as d3 from 'd3'

import {
  checkReglExtensions,
  createRegl,
  createTextureFromUrl,
  dist,
  getBBox,
  isRgb,
  isPointInPolygon,
  isRgba,
  toRgba,
  max,
  min
} from './utils'


const NOOP = () => {}

const creategraph = (options) => {
  let initialRegl = options.regl,

  canvas = options.canvas,
  drawNodes = options.createDrawNodes || NOOP;

  let size = [options.width || 500, options.height || 500]

  let state = {
    transform: {k: 1, x: 0, y: 0},
    ceiling: 40,
    pointSize: 10,
    scaling: .4,
    sizeAttenuation: .1,

    sentimentFilter: 0,
    numNodes: 1,
    showLines: true,
    showNodes: true,
    flatSize: true,
    edgeColors: true,
    selectedCluster: -1,
    favorites: [],
    dateFilter: [0,Infinity],
    projection:  new Float32Array([1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1]),
    model: new Float32Array([1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1]),
    hoveredPoint: -1,
    containerDimensions: (canvas).getBoundingClientRect(),
    size: size,
    selection: []

  };
  let w = size[0], h = size[1];


  let attributes = options.attributes;


const square_box = d3.min([w, h])


const scales = {};
for (let [name, dim] of [['x', w], ['y', h]]) {
  const buffer = (dim - square_box)/2
  scales[name] =
    d3.scaleLinear()
   .domain(d3.extent(options.data.nodes.map(d => d[name])))
   .range([buffer, dim-buffer])
}

    function zoomed(translate) {
      const {transform} = translate || d3.event

      // Just for display above the image.
      state.transform = transform

      drawRaf()

      // Three different *drawing* functions.

      // SVG can just have the transform applied directly to the existing elements.
      // d3.select(svg).selectAll("g").attr("transform", transform)
      // // Canvas has the transform applied inside the draw
      // draw_canvas(transform)
      //
      // // Regl clearing outside the rendering call.
      // regl.clear({color: [0, 0, 0, 0],depth: 1})
      // renderer({transform: transform})

      // Apply the zoom transform to the other elements.

      // This problem ends up calling the zoom behavior on all events three times per tick; it would be safer
      // to manage a separate zoom behavior for each element.
      // d3.selectAll(canvas)
      //   .filter(function(d) {
      //     // avoid recursing this call on the element that generated the call.
      //     return d3.zoomTransform(this) != transform
      //   })
      //   .call(zoom.transform, transform);

    }
    window.zoom = d3.zoom()
            .extent([[0, 0], [innerWidth, innerHeight]])
            //.scaleExtent([.5, 10])
            .on("zoom", zoomed)
            //.translateBy(d3.transition(), 500, 500)

  d3.select(options.canvas)
  .call(window.zoom)

  const scratch = new Float32Array(16);
  let mousePosition  = [0, 0];
  let pointList = []

  _.extend(state, options.initialState)

  let regl = initialRegl || createRegl(canvas)
  let mouseDown = false
  let mouseDownShift = false
  let mouseDownPosition = [0, 0]
  let selection = []

  let searchIndex
  let isViewChanged = false
  let isMouseInCanvas = false
  //let [updateCurves, drawCurves] = createCurves(options.regl, attributes, scales)
  let drawPointBodies = createDrawPoints(options.regl, attributes, scales)

  // Get a copy of the current mouse position
  const getMousePos = () => mousePosition.slice()
  const getNdcX = x => -1 + (x / size[0]) * 2
  const getNdcY = y => 1 + (y / size[1]) * -2

  // Get relative WebGL position
  const getMouseGlPos = () => [
    getNdcX(mousePosition[0]),
    getNdcY(mousePosition[1])
  ]

  const getScatterGlPos = (pos=getMouseGlPos()) => {
    const [xGl, yGl] = pos

    // Homogeneous vector
    const v = [xGl, yGl, 1]
    // projection^-1 * view^-1 * model^-1 is the same as
    // model * view^-1 * projection
    let mvp = mat3.invert(scratch,
      [state.transform.k, 0, state.transform.x,
      0, state.transform.k, state.transform.y,
      0, 0, 1],
    )

    vec3.transformMat3(v, v, mvp)
    //console.log(v)
    return v.slice(0, 2)
  }

  const raycast = () => {
    let pointSize = 1000; //scale to zoom level
    const [mouseX, mouseY] = getScatterGlPos()

    const scaling = 1

    const scaledPointSize =
      2 *
      pointSize *
      (min(1.0, scaling) + Math.log2(max(1.0, scaling))) *
      window.devicePixelRatio

    const xNormalizedScaledPointSize = scaledPointSize / size[0]
    const yNormalizedScaledPointSize = scaledPointSize / size[1]
    // Get all points within a close range
    const pointsInBBox = searchIndex.range(
      mouseX - xNormalizedScaledPointSize,
      mouseY - yNormalizedScaledPointSize,
      mouseX + xNormalizedScaledPointSize,
      mouseY + yNormalizedScaledPointSize
    )
    // Find the closest point
    let minDist = scaledPointSize
    let clostestPoint

    pointsInBBox.forEach(idx => {
      const {x, y} = searchIndex.points[idx]

      const d = dist(x, y, mouseX, mouseY)
      if (d < minDist && attributes.stateIndex[1] !== 0) {
        minDist = d
        clostestPoint = idx
      }
    })
    return clostestPoint
    if (minDist < (pointSize / size[0]) * 2) {
      return clostestPoint
    };


    return -1
  }

  const deselect = () => {
  }


  const select = (points) => {
    if (typeof points === 'number') state.selection = [points]
    else state.selection = points
    drawRaf() // eslint-disable-line no-use-before-define
  }

  let getRelativePosition = (pos) => {
    const rect = options.canvas.getBoundingClientRect()

    pos[0] = (pos[0] - rect.left ) /// devicePixelRatio
    pos[1] = (pos[1] - rect.top)  /// devicePixelRatio
    return [...pos]
  }

  const getRelativeMousePosition = event => {
    const rect = event.target.getBoundingClientRect()

    mousePosition[0] = (event.clientX - rect.left )// / devicePixelRatio
    mousePosition[1] = (event.clientY - rect.top)  /// devicePixelRatio

    return [...mousePosition]
  }

  const mouseDownHandler = event => {
    events['mousedown']()
    mouseDown = true

    mouseDownPosition = getRelativeMousePosition(event)
    mouseDownShift = event.shiftKey
  }

  const mouseUpHandler = () => {
    events['mouseup']()

    mouseDown = false
  }

  const mouseClickHandler = event => {
    events['click']()

    const currentMousePosition = getRelativeMousePosition(event)
    const clickDist = dist(...currentMousePosition, ...mouseDownPosition)
    const clostestPoint = raycast()
    if (clostestPoint >= 0) select([clostestPoint])

    if (event.shiftKey) {
      //updateCurves(pointList)
    }
  }

  const blurHandler = () => {
    events['blur']()
    state.hoveredPoint = -1;
    isMouseInCanvas = false;
    mouseUpHandler();
    drawRaf(); // eslint-disable-line no-use-before-define
  };

  const mouseMoveHandler = event => {
    events['mousemove']()

    let coordinates = getRelativeMousePosition(event)
    // Only ray cast if the mouse cursor is inside
    if (!mouseDownShift) {
      const clostestPoint = raycast()
      hover(clostestPoint)
      if (clostestPoint)
      events.hover(clostestPoint, pointList[clostestPoint], event, coordinates) // eslint-disable-line no-use-before-define
      else
      events.hoverOff()
    }
    if (mouseDown) drawRaf() // eslint-disable-line no-use-before-define
  }

  const setHeight = newHeight => {
    if (!+newHeight || +newHeight <= 0) return
    size[1] = +newHeight
    options.canvas.height = size[1] * window.devicePixelRatio
  }

  const setWidth = newWidth => {
    if (!+newWidth || +newWidth <= 0) return
    size[0] = +newWidth
    options.canvas.width = size[0] * window.devicePixelRatio
  }

  const setPoints = newPoints => {
    pointList = newPoints
    searchIndex = new KDBush(newPoints, p => p.x, p => p.y, 16)
    window.search = searchIndex
  }

  const draw = () => {
    //drawCurves(state)
    drawPointBodies(state);
    //state.screenshot = canvas.toDataURL("image/png", 1);
  }

  const drawRaf = withRaf(draw)

  const withDraw = f => (...args) => {
    const out = f(...args)
    drawRaf()
    return out
  }

  const refresh = () => {
    regl.poll()
  }

  const setSize = (size) => {
    let dpi = window.devicePixelRatio

    options.canvas.width =  dpi * size[0]
    options.canvas.height = dpi *  size[1]

    options.canvas.style.width = size[0] + 'px'
    options.canvas.style.height = size[1] + 'px'

    setHeight(size[0])
    setWidth(size[1])

    refresh()
    drawRaf()
  }

  const hover = (point) => {
    let needsRedraw = false

    if (point >= 0) {
      needsRedraw = true
      const newHoveredPoint = point !== state.hoveredPoint
      state.hoveredPoint = point

    } else {
      needsRedraw = state.hoveredPoint
      state.hoveredPoint = -1
      //if (+needsRedraw >= 0) options.deselect()
    }
    drawRaf()
    //if (needsRedraw) drawRaf(console.log(null))
  }

  const reset = () => {
    console.log('reset')
  }

  const mouseEnterCanvasHandler = () => {
    events['mouseenter']()
    isMouseInCanvas = true
  }

  const mouseLeaveCanvasHandler = () => {
    events['mouseleave']()
    hover()
    isMouseInCanvas = false
    drawRaf()
  }

  let wheelDelta= 0;


  let resizeHandler = () => {
    state.containerDimensions = (options.canvas).getBoundingClientRect()
    let rect = options.canvas.getBoundingClientRect()
    size[0] = rect.width
    size[1] = rect.height
    setHeight(height)
    setWidth(width)
  }

  const init = () => {
    setSize( size )
    window.addEventListener('blur', blurHandler, false);
    window.addEventListener('mousedown', mouseDownHandler, false)
    window.addEventListener('mouseup', mouseUpHandler, false)
    window.addEventListener('mousemove', mouseMoveHandler, false)
    canvas.addEventListener('mouseenter', mouseEnterCanvasHandler, false)
    canvas.addEventListener('mouseleave', mouseLeaveCanvasHandler, false)
    canvas.addEventListener('click', mouseClickHandler, false)
    window.addEventListener('resize', resizeHandler);
    setPoints(attributes.nodes) //create Index
  }

  const destroy = () => {
    canvas = undefined
    state.camera = undefined
    regl = undefined
  }
  init(canvas)

  const setState = (options) => {
    drawRaf()
    _.each(options, (k,v) => { state[v] = k })
  }

  let parseColor = (rgb) => {
    let c = d3.rgb(rgb)
    return [c.r /255 , c.g /255 , c.b /255];
  }

  let eachNode = (indices, property, fn) => {
    let list = Array.isArray(indices) ? indices: attributes.nodes.map((d,i) => i)

    list.forEach(idx => {
      fn(attributes[property][idx], attributes.nodes[idx])
    })
    drawRaf()
  }

  let setNodeColor = (indices, color) => {
    let list = Array.isArray(indices) ? indices: attributes.nodes.map((d,i) => i)
    list.forEach(idx => {
      attributes.color[idx] = parseColor(color)
    })
    drawRaf()
  }

  let setNodePosition = (indices, pos) => {
    let list = Array.isArray(indices) ? indices: attributes.nodes.map((d,i) => i)
    list.forEach(idx => {
      pos = 'function' == typeof pos ? pos(attributes.nodes[idx]) : pos
      attributes.position[idx][0] = pos[0]
      attributes.position[idx][1] = pos[1]
    })
    drawRaf()
  }

  let setNodeVisibility = (indices, val) => {
    let list = Array.isArray(indices) ? indices: attributes.nodes.map((d,i) => i)
    list.forEach(idx => {
      let show = 'function' == typeof val ? val(attributes.nodes[idx]) : val
      attributes.stateIndex[idx][1] = show
    })
    drawRaf()
  }

  let setNodeSize = (indices, size) => {
    indices.forEach(idx => {
      attributes.position[idx][2] = size
    })
    drawRaf()
  }

  let setNodeShape = (indices, shape) => {
    indices.forEach(idx => {
      attributes.stateIndex[idx][2] = - shape
    })
    drawRaf()
  }

  let noop = () => {}
  let events = {
    'blur' :noop,
    'mousedown' :noop,
    'mouseup' :noop,
    'mousemove' :noop,
    'mouseenter' :noop,
    'mouseleave' :noop,
    'hoverOff': noop,
    'click' :noop,
    'wheel': noop,
    hover: noop,
  }

  let on = (event, listener) => {
    events[event] = listener

  }

  let yes = _.extend(d3.zoomIdentity,  {
       "k": 552,
       "x": -258061,
       "y": -144814
     })

   d3.select(options.canvas).call(zoom.transform,   yes)
  return {
    setNodePosition: setNodePosition,
    state: state,
    eachNode: eachNode,
    brush: (selection, svg) => {
      let clipspace = function (pos) {
        return [2. * (pos[0] / width) - 1.,
        1. - ((pos[1] / height) * 2.)]
      }

       let p = selection.map(clipspace).map(getScatterGlPos)
      let x0 = p[0][0]
      let x1 = p[1][0]

      let y0 = p[0][1]

      let y1 = p[1][1]
      let c = 0;

      attributes.stateIndex.forEach((trip, idx) => {
        let {x, y} = searchIndex.points[idx];
        let inbox =  x > x0 && x < x1
          &&  y > y1 && y < y0
        if(inbox) c++
        trip[1] = inbox ? 10 : -10;
      })
      draw()
    },
    resetView: () => {
      state.camera.setView(mat4.clone(initialView))
      draw()
    },
    zoomToNode: (id) => {
      let pos = attributes.position[id]
      let xy = pos.slice(0,2)
      camera.lookAt(xy)
      draw()
    },
    setNodeColor,
    setNodeSize,
    setNodeShape,
    setNodeVisibility,
    deselect,
    destroy,
    on: on,
    draw: drawRaf,
    repaint: () => {
      withDraw(reset)();
    },
    hoverPoint: (uuid) => {
      state.hoveredPoint = pointList.findIndex(d => d.uuid === uuid)
      draw()
    },
    refresh,
    select,
    setState,
  }
}

const init = (props) => {
  props.attributes = processData(props.data)
  props.regl = createRegl(props.canvas)
  let graph = creategraph(props)
  graph._data = props

  return graph
}

export default { init }

export { createRegl, createTextureFromUrl }
