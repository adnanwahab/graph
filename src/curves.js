var Bezier = require('bezier-js')


import {window_transform} from './utils';
let roundCapJoin

function roundCapJoinGeometry(regl, resolution) {
  const instanceRoundRound = [
    [0, -0.5, 0],
    [0, -0.5, 1],
    [0, 0.5, 1],
    [0, -0.5, 0],
    [0, 0.5, 1],
    [0, 0.5, 0]
  ];
  // Add the left cap.
  for (let step = 0; step < resolution; step++) {
    const theta0 = Math.PI / 2 + ((step + 0) * Math.PI) / resolution;
    const theta1 = Math.PI / 2 + ((step + 1) * Math.PI) / resolution;
    instanceRoundRound.push([0, 0, 0]);
    instanceRoundRound.push([
      0.5 * Math.cos(theta0),
      0.5 * Math.sin(theta0),
      0
    ]);
    instanceRoundRound.push([
      0.5 * Math.cos(theta1),
      0.5 * Math.sin(theta1),
      0
    ]);
  }
  // Add the right cap.
  for (let step = 0; step < resolution; step++) {
    const theta0 = (3 * Math.PI) / 2 + ((step + 0) * Math.PI) / resolution;
    const theta1 = (3 * Math.PI) / 2 + ((step + 1) * Math.PI) / resolution;
    instanceRoundRound.push([0, 0, 1]);
    instanceRoundRound.push([
      0.5 * Math.cos(theta0),
      0.5 * Math.sin(theta0),
      1
    ]);
    instanceRoundRound.push([
      0.5 * Math.cos(theta1),
      0.5 * Math.sin(theta1),
      1
    ]);
  }
  return {
    buffer: regl.buffer(instanceRoundRound),
    count: instanceRoundRound.length
  };
}

function interleavedStripRoundCapJoin3DDEMO(regl, resolution, window_transformation_matrix, untransform_matrix) {
  roundCapJoin = roundCapJoinGeometry(regl, resolution);
  return regl({
    vert: `
      precision highp float;
      attribute vec3 position;
      attribute vec3 pointA, pointB;
      attribute vec3 colorA, colorB;

      uniform float width;
      uniform vec2 resolution;
      uniform mat4 view, projection;

      varying vec3 vColor;

      uniform float u_k;
      uniform mat3 u_window_scale;
      // Transform from the open window to the d3-zoom.
      uniform mat3 u_zoom;
      uniform mat3 u_untransform;

      mat3 from_coord_to_gl = u_window_scale * u_zoom * u_untransform;


      void main() {
        //vec3 pos2d = vec3(position.x, position.y, 1.0) * from_coord_to_gl;
        //gl_Position = vec4(pos2d, 1.0);
        vec3 clip0 = pointA * from_coord_to_gl;
        vec3 clip1 = pointB * from_coord_to_gl ;

        vec2 screen0 = resolution * (0.5 * clip0.xy/clip0.z + 0.5);
        vec2 screen1 = resolution * (0.5 * clip1.xy/clip1.z + 0.5);
        vec2 xBasis = normalize(screen1 - screen0);
        vec2 yBasis = vec2(-xBasis.y, xBasis.x);
        vec2 pt0 = screen0 + width * (position.x * xBasis + position.y * yBasis);
        vec2 pt1 = screen1 + width * (position.x * xBasis + position.y * yBasis);
        vec2 pt = mix(pt0, pt1, position.z);
        vec3 clip = mix(clip0, clip1, position.z);
        gl_Position = vec4(clip.z * (2.0 * pt/resolution - 1.0), clip.z, clip.z);
        vColor = mix(colorA, colorB, position.z);
      }`,

    frag: `
      precision highp float;
      varying vec3 vColor;
      void main() {
        gl_FragColor = vec4(vColor, 1);
      }`,

    cull: {
      enable: true,
      face: "back"
    },

    attributes: {
      position: {
        buffer: roundCapJoin.buffer,
        divisor: 0
      },
      pointA: {
        buffer: regl.prop("points"),
        divisor: 1,
        offset: Float32Array.BYTES_PER_ELEMENT * 0
      },
      pointB: {
        buffer: regl.prop("points"),
        divisor: 1,
        offset: Float32Array.BYTES_PER_ELEMENT * 3
      },
      colorA: {
        buffer: regl.prop("color"),
        divisor: 1,
        offset: Float32Array.BYTES_PER_ELEMENT * 0
      },
      colorB: {
        buffer: regl.prop("color"),
        divisor: 1,
        offset: Float32Array.BYTES_PER_ELEMENT * 3
      }
    },

    uniforms: {
      width: regl.prop("width"),
      model: regl.prop("model"),
      view: regl.prop("view"),
      projection: regl.prop("projection"),
      resolution: regl.prop("resolution"),
      u_k: function(context, props) {
        return props.transform.k;
      },
      u_window_scale: window_transformation_matrix,
      u_untransform: untransform_matrix,
      u_zoom: function(context, props) {
        return [
          // This is how you build a transform matrix from d3 zoom.
          [props.transform.k, 0, props.transform.x],
          [0, props.transform.k, props.transform.y],
          [0, 0, 1],
        ].flat()
      },
      u_size: 4,


    },

    cull: {
      enable: true,
      face: "back"
    },

    count: roundCapJoin.count,
    instances: regl.prop("segments"),
    viewport: regl.prop("viewport"),


  });
}

function createCurves (regl, attributes, scales) {
  let w = 1000, h = 500;

  const [window_transformation_matrix, untransform_matrix] = window_transform(scales.x, scales.y, w, h).map(d => d.flat())

  const drawCommand = interleavedStripRoundCapJoin3DDEMO(
    regl,
    16, window_transformation_matrix, untransform_matrix
  );

  let positions = []
  let getQuadraticControlPoint = function(x1, y1, x2, y2) {
     return {
       x: (x1 + x2) / 2 + (y2 - y1) / 4,
       y: (y1 + y2) / 2 + (x1 - x2) / 4
     };
   };
  let fillPosition = (d) => {
    let cp = getQuadraticControlPoint(d.x1, d.y1 , d.x2, d.y2)
    var curve = new Bezier(d.x1, d.y1 , cp.x, cp.y , d.x2, d.y2);
    var LUT = curve.getLUT(50);
    LUT.forEach(function(p) { positions.push([p.x, p.y, 1]) });
    positions.push(0,0,0)
  }

  attributes.edges.curves.forEach(fillPosition)
  let segments = 0
  let colors = positions.map( d => [Math.random(),Math.random(),Math.random()] )

  let pos = regl.buffer()
  let color = regl.buffer()

  let update =  (node, id) => {
      let connections = attributes.edges.edges.filter(edge => {
        if (! id) return true
        return edge.source == id || edge.target == id
      }).map(d => {
        let source = attributes.nodes[d.source], target = attributes.nodes[d.target];
        return {
          x1: (source.x),
          y1: (source.y),
          x2: (target.x),
          y2: (target.y),
        }
      })

      positions = []
      //ßconsole.log(positions)

      connections.forEach(fillPosition)

      segments=positions.length

      let colors = positions.map( d => [.3, .3, .3]  )
      pos({data: positions})
      color({data: colors})
    }

    let draw = (state) => {
      let dim = state.containerDimensions
      //console.log(state.size)
      let dpi = window.devicePixelRatio
      if (segments) drawCommand({


        points: pos,
        color: color,
        width: 1,
        model: out,
        view: out, //view,
        projection: out,
        resolution: [state.size[0] / dpi, state.size[1] / dpi ],
        segments: segments - 1,
        viewport: { x: 0, y: 0, width: state.size[0] * dpi, height: state.size[1] * dpi  },
      })
    }
    return [update, draw]
}

export default createCurves
