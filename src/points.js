import * as d3 from 'd3'
import {window_transform} from './utils';


const POINT_FS = `
#ifdef GL_OES_standard_derivatives
#extension GL_OES_standard_derivatives : enable
#endif
precision mediump float;

uniform vec2 selection;
//uniform sampler2D texture;
//uniform sampler2D texture2;


varying vec4 vColor;
varying vec3 borderColor;
varying float uv;
uniform vec2 resolution;
uniform float time;

float aastep(float threshold, float value) {
  #ifdef GL_OES_standard_derivatives
    float afwidth = length(vec2(dFdx(value), dFdy(value))) * 0.70710678118654757;
    return smoothstep(threshold-afwidth, threshold+afwidth, value);
  #else
    return step(threshold, value);
  #endif
}

void main() {


  // vec2 uv = vec2(gl_FragCoord.xy / resolution.xy) - 0.5;
  //
  // //correct aspect
  // uv.x *= resolution.x / resolution.y;
  //
  // //animate zoom
  // uv /= 1. ;//;sin(time * .2);
  //
  // //radial distance
  // float len = length(uv);
  //
  // //anti-alias
  // len = aastep(0.5, len);
  //
  // gl_FragColor.rgb = vec3(len) + .50;
  // gl_FragColor.a   = 1.0;


  // //
  // if (uv == 0.)
  // gl_FragColor = vColor;
  // else
  // gl_FragColor = texture2D(texture2, gl_PointCoord) * vColor;

  float r = 0.0, delta = 0.0, alpha = 1.0;
  vec2 cxy = 2.0 * gl_PointCoord - 1.0;
  r = dot(cxy, cxy);

  #ifdef GL_OES_standard_derivatives
    delta = fwidth( r);
    alpha = 1.0 - smoothstep(1.0 - delta, 1.0 + delta, r);
  #endif

    //vec3 color = vColor.rgb;
  //vec3 color =   (delta > 0.75) ? vColor.rgb : borderColor;
  //gl_FragColor = texture2D(texture, gl_PointCoord) * vColor;
  //gl_FragColor.a = alpha;

  float vSize = 1.0;
  float uEdgeSize = 2.;
  float distance = length(2.0 * gl_PointCoord - 1.0);


  float sEdge = smoothstep(
      vSize - uEdgeSize - 2.0,
      vSize - uEdgeSize,
      distance * (vSize + uEdgeSize)
  );
  gl_FragColor = vColor;
  distance = aastep(.5, distance);

  // if (distance > 1.0) {
  //     discard;
  // }
  //gl_FragColor.rgb = (vec3(.3) * sEdge) + ((1.0 - sEdge) * gl_FragColor.rgb);
  //if (distance > .6) gl_FragColor.rgb = vec3(0.);
  gl_FragColor.a *=  1. - distance;
}
`
const POINT_VS = `
precision mediump float;
uniform mat4 projection;
uniform mat4 model;
uniform mat4 view;


uniform float pointSize;
uniform float scaling;
uniform float sizeAttenuation;

attribute vec4 pos;
attribute vec3 color;
//cluster, visiblity, texture
attribute vec3 stateIndex;
attribute float dates;
attribute float sentiment;
attribute vec2 offset;

uniform float ceiling;

uniform float hoveredPoint;
//uniform float selectedPoint;
uniform vec2 dimensions;

uniform float selectedCluster;

uniform bool flatSize;

// variables to send to the fragment shader
varying vec4 vColor;
varying vec3 borderColor;
varying float uv;

uniform float u_k;
uniform mat3 u_window_scale;
// Transform from the open window to the d3-zoom.
uniform mat3 u_zoom;
uniform mat3 u_untransform;


vec2 clipspace (vec2 pos) {
  return vec2(
    -1. + (pos[0] / dimensions[0]) * 2.0,
    1. + (pos[1] / dimensions[1]) * -2.0
  );
}

void main() {
  mat3 from_coord_to_gl = u_window_scale * u_zoom * u_untransform;

  vec2 position = clipspace(pos.xy);

  vec3 pos2d = vec3(position.x, position.y, 1.0) * from_coord_to_gl;
  gl_Position = vec4(pos2d, 1.0);

  vColor = vec4(color, 1);
  float finalScaling = 2.;
  finalScaling += .1 + pow(pos.z, scaling * 1.);

  if (pos.w == hoveredPoint) vColor.xyz -= .2;
  //if (pos.w == selectedPoint) vColor.xyz -= .3;
  //if (pos.w == selectedPoint) vColor.a = 1.;

  vColor.a = .7;
  if ( (stateIndex[1] == -10.)) vColor.a = .1;

  gl_PointSize = min(pointSize + (exp(log(finalScaling)*sizeAttenuation * .01)), ceiling);
}
`

//
// const getNdcX = x => -1 + (x / innerWidth) * 2
// const getNdcY = y => 1 + (y / innerHeight) * -2



let createDrawPoints = (regl, attributes, scales) => {
  let w = innerHeight, h = innerHeight;
  const [window_transformation_matrix, untransform_matrix] = window_transform(scales.x, scales.y, w, h).map(d => d.flat())

  let schema = {
        pos: {
          //xy size
          buffer: () => attributes.position,
          size: 4
        },
        color: {
          buffer: () => attributes.color,
          size: 3

        },
        stateIndex: {
          buffer: () => attributes.stateIndex,
          size: 3
        },
      }

return  regl({
    frag: POINT_FS,
    vert: POINT_VS,
    depth: {
  enable: false,

  },

  blend: {
  enable: true,
  func: {
   srcRGB: 'src alpha',
   srcAlpha: 1,
   dstRGB: 'one minus src alpha',
   dstAlpha: 1
  },
  equation: {
   rgb: 'add',
   alpha: 'add'
  },
  color: [0, 0, 0, 0]
  },

  attributes: schema,

    uniforms: {
      ceiling: regl.prop('ceiling'),
      time: (context) => { return console.log(context.time) || context.time },
      resolution: [innerWidth, innerHeight],
      hoveredPoint: regl.prop('hoveredPoint'),
      dimensions: [window.innerWidth, window.innerHeight],
      scaling: regl.prop('scaling'),
      pointSize: regl.prop('pointSize'),
      pointSizeExtra: () => 1,
      sizeAttenuation: regl.prop('sizeAttenuation'),
      flatSize: regl.prop('flatSize'),

      u_k: function(context, props) {
        return props.transform.k;
      },
      u_window_scale: window_transformation_matrix,
      u_untransform: untransform_matrix,
      u_zoom: function(context, props) {
        return [
          // This is how you build a transform matrix from d3 zoom.
          [props.transform.k, 0, props.transform.x],
          [0, props.transform.k, props.transform.y],
          [0, 0, 1],
        ].flat()
      },
      u_size: 4
      //texture: () => textures[0],
      //texture2: () => textures[1]
    },
    count: attributes.position.length,
    primitive: 'points'
  })


};



// let circleImg = new Image(
// )
//
// let starImg = new Image()
// starImg.src = starSprite;
//
// circleImg.src = circleSprite;
//
// var emptyTexture = regl.texture({
//   shape: [16, 16]
// })
//
//
// let textures = [emptyTexture, emptyTexture]
//
// circleImg.onload = () => {
//   textures[0] = regl.texture({premultiplyAlpha: true, data: circleImg})
// }
// if (circleImg.complete) circleImg.onload()
//
// starImg.onload = () => {
//   textures[1] = regl.texture(starImg)
// }
// if (starImg.complete) starImg.onload()

export default createDrawPoints
